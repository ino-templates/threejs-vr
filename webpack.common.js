const path = require("path");

module.exports = {
  entry: {
    app: "./src/index.ts",
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  // target: ["web", "es5"], // -> https://github.com/webpack/webpack-dev-server/issues/2812
};
