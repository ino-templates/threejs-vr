const path = require("path");
const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  mode: "development",
  output: {
    path: path.resolve(__dirname, "public/dist"),
    publicPath: "/dist/",
    filename: `[name].dev.js`,
  },
  devtool: "inline-source-map",
  devServer: {
    open: true,
    contentBase: path.resolve(__dirname, "public"),
    publicPath: "/dist/",
    inline: true,
    hot: true,
  },
});
