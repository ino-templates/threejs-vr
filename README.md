# WebXR 開発用 テンプレート

|                                                             |                                                             |
| ----------------------------------------------------------- | ----------------------------------------------------------- |
| ![image1](docs/imgs/com.oculus.vrshell-20201111-111605.jpg) | ![image2](docs/imgs/com.oculus.browser-20201111-111633.jpg) |

## 環境

- TypeScript: v4.0.5
- Webpack: v5.4.0
- Three.js: v0.122.0
- Windows Subsystem for Linux 2

## Setup

1. 各種インストール

```sh
$ yarn install
```

2. ビルド (production)

```sh
$ yarn run build
```

3. 開発用サーバ立ち上げ(w/ hot reload)

```sh
$ yarn run develop
```

4. HTTP サーバ立ち上げ

```
$ yarn run httpd
```

## Oculus Quest 2 との接続

Oculus Browser v12.1.0 で確認済み

1. adb のインストール

Oculus Quest は基本的に Android 端末として構成されている。
詳細は <https://developer.android.com/studio/releases/platform-tools>

2. [optional] WSL2 でエイリアスを作成

WSL2 内で Android SDK をインストールしても接続端末が読み込まれないことがある(マウントすればいけるかも。要検証。)ので、Windows 側の `adb` を呼び出す。

Windows でのインストールが正常に完了していれば、PowerShell で以下のコマンドを打つとパスが表示される。

```powershell
> gcm adb | fl
```

3. Oculus Quest2 の接続

OQ2 を USB 接続する。OQ2 でポップアップが表示されるので「許可」を押す。

ターミナルで以下のように表示されれば接続済みであることが確認できる。

```sh
$ adb devices
List of devices attached
xxxxxxxxxxxxxx  device
```

4. ポートフォワーディング

OQ2 端末から開発 PC に向けてポート転送を行う。

```sh
$ adb reverse tcp:8080 tcp:8080
```
